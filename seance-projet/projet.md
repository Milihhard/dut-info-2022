# Projet

Vous devrez développer une application web complète en Javascript.  
Le but de ce projet est de créer un application en full stack.  
Nous allons alors avoir avec un projet en deux parties :
- Le front avec `Vue.js`
- Le back avec `fastify` et `Sequelize`

> Le front est responsable de l'aspet visuel. Le back est responsable du lien avec la base de données.

## Sujet
Un collectionneur vous a demandé de gérer son site.
> Ca peut être un collectionneur de n'importe quoi (art, objet de film, voiture,... Soyez créatif). 

Le gérant souhaiterait avoir un site lui permettant d'afficher toute sa collection et l'administrer (en ajouter, modifier, supprimer).

## Cahier des charges
> Il n'y a pas d'ordre particulier dans l'execution.

L'application doit contenir ces pages:
- Page listant les objets de la collection
- Page détaillant un objet sélectionné
- Page de création d'un objet
- Page d'édition d'un objet
> Un objet de la collection contient un nom, un type, une description, une image et une estimation du prix

### Fonctionnalités supplémentaires souhaitées
Le collectionneur aimerait ces fonctionnalité en plus dans son application :
- Pouvoir supprimer un objet de sa collection (par la page de listing ou de détail)
- La page listant les objets de sa collection devra être paginée
- La page listant les objets devra être filtré par le type
- La page de listing doit contenir la valeur estimée de la collection
- Il faut être connecté pour accéder à l'administration (création, modification, suppression). Une personne non conectée ne peut voir que la liste d'objets et le détail d'un objet

# Notation

La notation est sur plusieurs parties.
- Le contenu (Combien de fonctionnalités ont été faites)
- Le compréhension des différents outils (JS, Vue, Fastify, Sequelize)
- L'architecture de l'application (par exemple, la façon dont a été géré les composants, l'api).

> Le style n'est pas noté, vous pouvez même utiliser une librairie gérant le style (comme [Quasar](https://quasar.dev/) ou [Element plus](https://element-plus.org/))

Le projet devra être sur git (gitlab.com, gitlab du dut ou github)