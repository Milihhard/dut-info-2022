# Fastify
## Créer un projet
```sh
npm install fastify-cli --global
fastify generate nom_du_projet
```

## Gérer une route

```js
export default async function (fastify, opts) {
  // Défini une route POST sur /test/monId?color=rouge
  fastify.post('/test/:id', async function (request, reply) {
    request.body;
    request params;   // { id: 'monId' }
    request.query     // {ccolor: 'rouge' }
    return {
      text: 'test'
    }
  })
}
```

## Utiliser SQL

Nous utilisons la librairie sequelize
```sh
npm i sequelize mysql2
```

### associer à la base
```js
import { DataTypes, Sequelize } from 'sequelize';

const sequelize = new Sequelize('mysql://user:password@127.0.0.1:3306/db')
try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}
```
### Définir le model

```js
// Définition du model User
const User = sequelize.define('User', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING
    }
});
// Création de la table si elle n'existe pas
User.sync()
```

### Queries

```js
// Récupère tous les Users
const users = await User.findAll();

// Récupère tous les Users dont le prénom est Emilie
const users = await User.findAll({
  where: {
    firstName: 'Emilie'
  }
});

// Création d'un User
const user = await User.create({ firstName: body.firstname, lastName: body.lastname });

// Suppression des Users dont le prénom est Emilie
const user = await User.create({
  where: {
    firstName: 'Emilie'
  }
});
```


