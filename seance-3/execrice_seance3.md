# Séance 3

## Fastify
> Toutes les routes avec le meme nom/verbe sont la même route
1. Connecter la bdd a JS
1. Créer un model Task contentant un nom est son état (booleen)
1. Créer une route GET `/tasks` permettant de récupérer toutes les taches
1. Créer une route GET `/tasks/monId` permettant de récupérer une tache par son id
1. Créer une routes POST `/tasks` permettant de créer une tache avec un nom (état à faux)
1. Rajouter un controle sur le nom
1. Créer une route GET `/tasks?name=Taskname` permettant de récupérer toutes les taches avec le nom 'TaskName'
1. Créer une route PUT `/tasks` permettant de changer le nom d'une tache
1. Créer une route PUT `/tasks` permettant de changer le l'état d'une tache
1. Créer une route GET `/tasks/stats` permettant d'avoir le stats sur les taches (nombre de tache réalisé, total, % de tache réalisés)


## Full Stack
Reprendre l'application de la séance 2 et faire que les taches viennent du backend (fastify) et que l'on puisse créer des taches

