'use strict'

import example from "./example/index.js"

export default async function (fastify, opts) {
  fastify.get('/', async function (request, reply) {
    return { root: true }
  })
  fastify.register(example, {prefix: 'example'})
}
