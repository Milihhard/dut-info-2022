# Interagir avec le back
Pour lancer des requêtes HTTP, nous utilisons axios

```sh
npm i axios
```

## Utiliser axios
```js
import axios from 'axios';

// Requête GET sur 'example.com/ma-route/42'
axios.get('example.com/ma-route/42')
// Requête GET sur 'example.com/ma-route/42?name=Emilien'
axios.get('example.com/ma-route/42', {params: {name: 'Emilien'})

// Requête POST avec un body sur 'example.com/ma-route/42'
axios.post('example.com/ma-route/42', {info: 'info'})
// Requête POST avec un body sur 'example.com/ma-route/42?name=Emilien'
axios.post('example.com/ma-route/42', {info: 'info'}, {params: {name: 'Emilien'})
```

## Que faire après une requête Axios
Axios utilise des `Promise`

On a deux façons de le traiter
- En chainant
```js
axios.get('example.com/ma-route/42').then(response => {
  // Si la requête réussi
  console.log(response.data)
}).catch(err => {
  // Si la requête échoue
})
```
- En utilisant async/await
```js
try {
  const response = await axios.get('example.com/ma-route/42')
  // Si la requête réussi
  console.log(response.data)
} catch(err) {
  // Si la requête échoue
}
```

/!\ En utilisant `await` il faut passer les méthodes l'utilisant en `async`
```js
async function getMaRoute(id) {
  return await axios.get('example.com/ma-route/42')
}
try {
  const response = getMaRoute(42);
  // Si la requête réussi
  console.log(response.data)
} catch(err) {
  // Si la requête échoue
}
```