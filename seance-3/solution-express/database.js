const { DataTypes, Sequelize } = require('sequelize');

const sequelize = new Sequelize('mysql://user:password@127.0.0.1:3306/db')
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

const Task = sequelize.define('Task', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    state: {
        type: DataTypes.BOOLEAN
    }
});
Task.sync();

module.exports = {
    Task
}