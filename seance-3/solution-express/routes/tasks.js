const express = require('express');
const router = express.Router();
const { Task } = require('../database');
router.get('/', async function (request, res) {
  if (request.query.name)
    res.send(await Task.findAll({
      where: {
        name: { [Op.substring]: request.query.name }
      }
    }))
  else
    res.send(await Task.findAll());
})
router.get('/stats', async function (request, res) {
  const tasks = await Task.findAll();
  const total = tasks.length;
  const done = tasks.filter(task => task.state).length;
  const avg = this.total > 0 ? this.done / this.total * 100 : 0;
  res.send({
    done,
    total,
    avg,
  })
})
router.get('/:id', async function (request, res) {
  res.send(await Task.findByPk(request.params.id));
})
router.post('/', async function (request, res) {
  if (!request.body?.name) {
    res.status(400).send('Le nom est obligatoire')
  } else {

    res.send(await Task.create({
      name: request.body.name,
      state: false
    }))
  }
})
router.put('/:id', async function ({ params, body }, res) {
  const id = params.id;
  let changes = {};
  if (!body || !body.name && !body.state) {
    res.send({});
  } else {
    if (body.name) changes.name = body.name;
    if (body.state) changes.state = body.state;
    Task.update(body, {
      where: {
        id
      }
    })
    res.send("Mis à jour")
  }
});
router.delete('/:id', async function ({ params, body }, res) {
  const id = params.id;
  Task.destroy({
    where: {
      id
    }
  })
  res.send("Tache supprimé")
});

module.exports = router;
