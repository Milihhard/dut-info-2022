console.log("**** Utilisateur ****");

const showUser = (user) => `${user.civilite} ${user.prenom} ${user.nom}`;
console.log(showUser({ civilite: "He", prenom: 'Emilien', nom: 'Nicolas' }));
const showUsers = (users) => users.map(showUser);
console.log(showUsers([{ civilite: "He", prenom: 'Emilien', nom: 'Nicolas' }, { civilite: "She", prenom: 'Emilie', nom: 'Nicolas' }]));

console.log("**** Panier ****");
class Aliment {
    constructor(name, type, price) {
        this.name = name;
        this.type = type;
        this.price = price
    }
}
class Panier {
    constructor() {
        this.aliments = []
    }
    add(...aliment) {
        this.aliments.push(...aliment);
    }
    getAll() {
        return this.aliments;
    }
    count() {
        return this.aliments.length;
    }
    remove(name) {
        this.aliments.splice(this.aliments.findIndex(aliment => aliment.name === name), 1)
    }
    byType(type) {
        return this.aliments.filter(aliment => aliment.type === type);
    }
    getPriceAliment(name) {
        const alimentToFind = this.aliments.find(aliment => aliment.name === name);
        return alimentToFind ? alimentToFind.price : 0
    }
    getPricePanier() {
        return this.aliments.reduce((acc, aliment) => acc + aliment.price, 0);
    }
}

const panier = new Panier();

panier.add(new Aliment('courge', 'légume', 1), new Aliment('pomme', 'fruit', 2), new Aliment('banane', 'fruit', 3));
console.log('count', panier.count());
panier.remove('pomme');
console.log('count', panier.count());
panier.add(new Aliment('pomme', 'fruit', 2))
console.log('byType', panier.byType('fruit'));
console.log('price pomme', panier.getPriceAliment('pomme'));
console.log('price panier', panier.getPricePanier());

console.log('**** Moyenne ****');
const avg = (...notes) => notes.length > 0 ? notes.reduce((acc, note) => acc + note, 0) / notes.length : 0;
console.log('avg', avg(1, 2, 3, 4, 5, 6));
console.log('**** Pyramide ****');
const pyramide = (size) => {
    for (let i = 1; i <= size; i++) {
        console.log(new Array(i).fill('*').join(''));
    }
}
pyramide(4)
console.log('**** La majuscule! ****');
const toUppercase = (text) => text.split('.').map(line => {
    const l = line.trim();
    return l.charAt(0).toUpperCase() + l.slice(1)
}).join('. ')
console.log(toUppercase('lorem ipsum dolor sit amet, consectetur adipiscing elit. nullam vestibulum luctus dapibus. aenean molestie efficitur tempus. praesent maximus blandit est, et egestas urna ullamcorper ut. duis in accumsan arcu. quisque lobortis ligula nec sapien dictum, ut accumsan justo lobortis. morbi id molestie sem. morbi in ante vitae risus finibus rhoncus. nullam laoreet dictum sapien a condimentum. phasellus dapibus mauris sit amet felis consectetur iaculis. pellentesque dignissim tincidunt convallis. proin id dictum nisl.'));
console.log('**** Belle pyramide ****');
const beautifulPyramide = (size) => {
    for (let i = 0; i < size; i++) {
        console.log(
            new Array(size - i).fill(' ').join('') +
            new Array(1 + i * 2).fill('*').join('')
        );
    }
}
beautifulPyramide(4);

console.log('**** Pile & File ****');
class Pile {
    constructor() {
        this.list = [];
    }
    add(item) {
        this.list.push(item)
    }
    remove() {
        this.list.pop()
    }
}
const pile = new Pile();
pile.add(1);
pile.add(2);
console.log(JSON.stringify(pile));
pile.remove()
console.log(JSON.stringify(pile));
class Queue {
    constructor() {
        this.list = [];
    }
    add(item) {
        this.list.push(item)
    }
    remove() {
        this.list.shift()
    }
}
const file = new Queue();
file.add(1);
file.add(2);
console.log(JSON.stringify(file));
file.remove()
console.log(JSON.stringify(file));


console.log('**** Animaux ****');
class Animal {
    constructor(nom, type, nbPaw) {
        this.nom = nom;
        this.type = type;
        this.nbPaw = nbPaw;
    }
    cri() {
        console.log("J'hurle");
    }
}
const chien1 = new Animal('puppy', 'chien', 4);
class Chien extends Animal {
    constructor(nom) {
        super(nom, 'chien', 4);
    }
    cri() {
        console.log('Cri du chien');
    }
}
class Chat extends Animal {
    constructor(nom) {
        super(nom, 'chat', 4);
    }
    cri() {
        console.log('Cri du chat');
    }
}
class Poule extends Animal {
    constructor(nom) {
        super(nom, 'poule', 2);
    }
    cri() {
        console.log('Cri de la poule');
    }
}
class Cheval extends Animal {
    constructor(nom) {
        super(nom, 'cheval', 4);
    }
    cri() {
        console.log('Cri du cheval');
    }
}

const animals = [new Chien('puppy'), new Chat('matou'), new Poule('roger'), new Cheval('Pile Poil'), new Chien('gaby')];
console.log('cri all');
const criAll = animals => animals.forEach(animal => animal.cri());
criAll(animals)
console.log('cri by type');
const criByType = (animals, type) => criAll(animals.filter(animal => animal.type === type))
criByType(animals, 'cheval')
const nbPaws = animals => animals.reduce((acc, animal) => acc + animal.nbPaw, 0);
console.log('nbPaw', nbPaws(animals));
const sortByPaw = (animals) => animals.sort((a1, a2) => a1.nbPaw - a2.nbPaw);
console.log('sortByPaw', sortByPaw(animals));
const groupAnimals = (animals) => {
    const groupedAnimals = {};
    animals.forEach(animal => {
        if (groupedAnimals[animal.type]) {
            groupedAnimals[animal.type].push(animal)
        } else {
            groupedAnimals[animal.type] = [animal]
        }
    })
    return groupedAnimals;
}
console.log('groupAnimals', groupAnimals(animals));

console.log('**** Fibonacci ****');
const fibonacci = (aim, n1 = 1, n2 = 1) => {
    if (aim <= 1) return 1;
    if (aim <= n1) return n1;
    return fibonacci(aim, n2, n1 + n2);
}
console.log(fibonacci(1));
console.log(fibonacci(3));
console.log(fibonacci(7));