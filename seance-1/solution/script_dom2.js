const taches = document.getElementById("taches");
const statNode = document.getElementById("total");
class Tache {
  constructor(name) {
    this.name = name;
    this.isCheck = false;
  }
}
class TacheManager {
  taches = [];
  removeAllTaches() {
    while (taches.firstChild) {
      taches.removeChild(taches.lastChild);
    }
  }

  renderAllTaches() {
    this.removeAllTaches();

    this.taches.forEach((tache, index) => {
      this.renderTache(tache, index);
    });
    this.showStat();
  }

  renderTache(tache, index) {
    const tacheNode = document.createElement("p");
    const tacheNameNode = document.createElement("span");
    const tacheCheckNode = document.createElement("img");
    tacheCheckNode.src = tache.isCheck ? "./check.svg" : "./unchecked.svg";
    const tacheContent = document.createTextNode(tache.name);
    tacheNameNode.appendChild(tacheContent);
    tacheNameNode.addEventListener("click", () => {
      this.removeTache(index);
    });
    tacheCheckNode.addEventListener("click", () => {
      this.toggleCheck(index);
    });
    if (tache.isCheck) {
      tacheNameNode.classList.add("-checked");
    }
    tacheNode.appendChild(tacheCheckNode);
    tacheNode.appendChild(tacheNameNode);

    taches.appendChild(tacheNode);
  }
  addTache(tacheName) {
    this.taches.push(new Tache(tacheName));
    this.renderAllTaches();
  }

  removeTache(index) {
    this.taches.splice(index, 1);
    this.renderAllTaches();
  }
  toggleCheck(index) {
    this.taches[index].isCheck = !this.taches[index].isCheck;
    this.renderAllTaches();
  }
  showStat() {
    statNode.innerText = `Tâches : ${this.numberOfChecked} / ${this.total} (${
      this.total > 0 ? Math.round((this.numberOfChecked / this.total) * 100) : 0
    }%)`;
  }

  get total() {
    return this.taches.length;
  }
  get numberOfChecked() {
    return this.taches.filter((tache) => tache.isCheck).length;
  }
}

const tacheManager = new TacheManager();
tacheManager.showStat();

document.getElementById("addTache").addEventListener("submit", (e) => {
  e.preventDefault();
  tacheManager.addTache(document.getElementById("tacheInput").value);
  document.getElementById("tacheInput").value = "";
});
