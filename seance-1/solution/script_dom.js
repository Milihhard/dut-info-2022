const taches = document.getElementById("taches");
const statNode = document.getElementById("total");
let total = 0;

function getNumberOfChecked() {
  return document.querySelectorAll('[src="./check.svg"]').length;
}

function showStat() {
  statNode.innerText = `Tâches : ${getNumberOfChecked()} / ${total} (${
    total > 0 ? Math.round((getNumberOfChecked() / total) * 100) : 0
  }%)`;
}

document.getElementById("addTache").addEventListener("submit", (e) => {
  e.preventDefault();
  addTache(document.getElementById("tacheInput").value);
  document.getElementById("tacheInput").value = "";
});
showStat();

function addTache(tacheName) {
  const tacheNode = document.createElement("p");
  const tacheNameNode = document.createElement("span");
  const tacheCheckNode = document.createElement("img");
  tacheCheckNode.src = "./unchecked.svg";
  const tacheContent = document.createTextNode(tacheName);
  tacheNameNode.appendChild(tacheContent);
  tacheNameNode.addEventListener("click", (event) => {
    taches.removeChild(event.target.parentNode);
    total--;
    showStat();
  });
  tacheCheckNode.addEventListener("click", () => {
    if (tacheNameNode.style.textDecoration === "") {
      tacheNameNode.style.textDecoration = "line-through";
      tacheNameNode.style.color = "gray";
      tacheCheckNode.src = "./check.svg";
    } else {
      tacheNameNode.style.textDecoration = "";
      tacheNameNode.style.color = "";
      tacheCheckNode.src = "./unchecked.svg";
    }
    showStat();
  });
  tacheNode.appendChild(tacheCheckNode);
  tacheNode.appendChild(tacheNameNode);

  taches.appendChild(tacheNode);
  total++;
  showStat();
}
