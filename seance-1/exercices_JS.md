# Exercices

## Avant de commencer
Pour afficher le résultat, vous pouvez utiliser `console.log`
```js
const result=5
console.log('mon résultat est ' + 5)
console.log('mon résultat est', 5)
```

## Utilisateur
Un utilisateur est un objet contenant un prénom, un nom, une civilité et un âge.

1. Faire une fonction qui prend en paramètre un utilisateur et qui retourne une chaine de caractère contenant la civilité, le nom et le prénom afin de l'afficher
2. Faire une fonciton qui prend un tableau d'utilisateur et qui retourne le détail de chaque utilisateur

## Panier
Cet exercice est en plusieurs étapes

1. Créer une classe Aliment contenant un nom et un type d'aliment (fruit, légume, etc)
1. Créer une classe Panier contenant une liste d'aliments (vide au début)
1. Ajouter à la classe Panier une méthode permettant d'ajouter un aliment au panier
1. Ajouter à la classe Panier une méthode permettant de récupérer les aliments du panier
1. Ajouter à la classe Panier une méthode permettant d'avoir le nombre d'aliments dans le panier
1. Ajouter à la classe Panier une méthode permettant d'ajouter plusieurs aliments au panier
1. Faire que la fonction d'ajout d'un ou plusieurs aliment soit la même méthode
1. Ajouter à la classe Panier une méthode permettant d'enlever un aliment du panier (on se basera sur le nom de l'aliment)
1. Ajouter à la classe Panier une méthode permettant de récupérer le nom de tous les élements d'un type

Exemple: 
``` js
const panier = new Panier()
panier.addAliments(
    new Aliment("pomme", "fruit"),
    new Aliment("poire", "fruit"),
    new Aliment("peche", "fruit"),
    new Aliment("abricot", "fruit"),
    new Aliment("concombre", "legume"),
    new Aliment("courgette", "legume"),
    new Aliment("salade", "legume")
);
panier.getAlimentsByType("fruit")
// retourne ["pomme", "poire", "peche", "abricot"]
```
10. Maintenant chaque aliment a un prix. L'ajouter à la classe Aliment
10. Ajouter à la classe Panier une méthode permettant de connaitre le prix d'un aliment du panier en lui donnant son nom
> La méthode retourne 0 si l'aliment n'existe pas
12. Ajouter à la classe Panier une méthode permettant de connaitre le prix du panier
> La méthode retourne 0 si le panier est vide
## Calculer une moyenne
Créer une fonction qui prend N notes (ou un tableau de notes) et qui en fait la moyenne.
## Pyramide
Créer une fonction qui prend en paramètre la taille de la pyramide et qui la dessine.

Par exemple: 
> pyramide(4)
```
*
**
***
****
```

## La majuscule!
Prendre le texte suivant:
> lorem ipsum dolor sit amet, consectetur adipiscing elit. nullam vestibulum luctus dapibus. aenean molestie efficitur tempus. praesent maximus blandit est, et egestas urna ullamcorper ut. duis in accumsan arcu. quisque lobortis ligula nec sapien dictum, ut accumsan justo lobortis. morbi id molestie sem. morbi in ante vitae risus finibus rhoncus. nullam laoreet dictum sapien a condimentum. phasellus dapibus mauris sit amet felis consectetur iaculis. pellentesque dignissim tincidunt convallis. proin id dictum nisl.

Rajouter la majuscule à chaque début de phrase 



## Une plus belle pyramide
Créer une fonction qui prend une paramètre la taille de la pyramide et qui la dessine, centré.
Par exemple: 
> pyramideBelle(4)
```
   *
  ***
 *****
*******
```
> Une string a des méthode associés permettant de travailler facilement avec
> Par exemple le méthode `repeat` permet de répéter la string
```js
const monText = 'Hello World!';
console.log(monText.repeat(3)) // Hello World!Hello World!Hello World!
console.log('Hello World!'.repeat(3)) // Hello World!Hello World!Hello World!
```


## Pile et File

Faire deux classe:
- Un permettant de gérer une pile
- Un permettant de gérer une file

## Animaux

1. Créer une classe animal qui contient un nom, un type, un nombre de patte et une fonction cri
1. De cette classe, créer un chien
1. Créer une classe chien qui hérite de animal (le chien fait 'Ouaf')
1. Créer un chien est maintenant plus facile
1. Créer une classe Chat, Poule et Cheval
1. Créer une fonction qui prend un tableau d'animaux et qui les fait crier un par un
1. Créer une fonction qui prend un tableau d'animaux et qui fait crier qu'un type d'animal donné en paramètre
1. Créer une fonction qui prend un tableau d'animaux et le nombre de pattes total
1. Créer une fonction qui trie un tableau animaux par nombre de pattes.
1. Créer une fonction qui prends un tableau d'animaux et qui les regroupe
>exemple :
```js
const animaux = [
    new Chien('Gaby'),
    new Chat('Matou'),
    new Chien('Nestor'),
    new Chat('Blanchette'),
    new Cheval('Roger')
]
console.log(group(animaux))

// group renvoie:
{
    chien: [{
    //chien Gaby
    },{
    //chien Nestor
    }],
    chat: [{
        // Chat matou
    }],
    cheval: [{
        // Cheval Roger
    }]
}
```
## Suite de fibonacci
Créer une fonction qui prend en paramètre le nombre et qui retourne le nombre le plus proche dans la suite de fibonacci

> Exemple:
```js
fibonacci(3) // retourne 3
fibonacci(7) // retourne 8
```
