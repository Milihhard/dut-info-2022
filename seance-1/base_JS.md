
# Javascript

## Commentaire
 ``` js
 // C'est un commentaire

 /*
   C'est une commentaire
   sur plusieurs lignes
 */
```
## Definition d'une variable 


Ancienne façon de définir une variable (à éviter)
```js
 var maVariable = true
```
 
 Déclaration d'une variable
```js

 let maVariable = true
```
 
Déclaration d'une constante

```js
const maConstante = true
 ```

 ## Types primitifs

 ``` js
 //Booléen
 let b = true
 // Null
 let b = null
//Undefined
let b = undefined
//Nombre
let n = 42
//Chaîne de caractères (String)
let text = 'Lorem Ipsum'
let text2 = "Lorem Ipsum"
```

### Types Avancés (Anecdotique)
```js
// Nombre très grand
const bigNumber = BigInt(42)
// Symbole
const symbol = Symbol();
 ```
## Conditions et boucles
### Condition 
```js
if(condition) {
    //action si vrai
} else if(action) {
    //action si vrai
} else {
    //action si faux
}

// Ternaire
condition ? vrai : faux
// Exemple
const argent = estPaye ? 1000 : 0 
```
 ### Boucles
 ```js
 for(let i = 0; i <10; i++) {
    // Faire quelque chose
 }
 ```
 ```js
 while(valeurEstVrai) {
    // Faire quelque chose
 }
 ```
## Objet

> Tout élément n'étant pas de type primitif est un objet

Création d'un objet
```js
const monObjet = {}
```
Création d'un objet avec des valeurs
```js
const monObjet = {
    nom: "Bob",
    age: "18"
}
```
Récupération d'une valeur
```js
monObjet.nom
monObjet['nom']
```
Modification d'un élément
```js
monObjet.aimeLesFrites = true
```
Suppression d'un élément
```js
delete monObjet.aimeLesFrites
```

## String
Concaténation de deux strings
```js
const textFinal = 'Hello ' + 'World!' + 2
// le 2 sera converti en string
```
Concaténation avancée
```js
const age = 18
console.log(`j'ai ${age} ans`) // j'ai 18 ans
```
Passer un string en majuscule
```js
"en minuscule".toUpperCase() // EN MINUSCULE
```
Passer un string en minuscule
```js
"EN MAJUSCULE".toLowerCase() // en majuscule
```
Récupérer une partie d'un texte
```js
const text = "Hello World"
test.substring(1, 4) // ell
test.substring(4) // o World
```
 ## Tableau
 ### Déclaration
 ```js
 const monTableau = []
```
 Déclaration d'un tableau avec des valeurs
```js
const monTableauRempli = [1, 2, 3, 4, 5]
```
### Accéder à un élément du tableau
```js
const element = monTableauRempli[0]
```
> Attention: l'index commence à 0

### Action sur un tableau
Ajouter un élément au tableau
```js
monTableau.push(10)
```
Ajouter un élément au début du tableau
```js
monTableau.unshift(10)
```
Enlever le dernier élément du tableau
```js
monTableau.pop()
```
Enlever le premier élément du tableau
```js
monTableau.shift()
```
Enlever un élément du tableau
```js
monTableau.splice(0, 1)
```
> Le premier paramètre est l'index de l'élément
> Le deuxième paramètre est le nombre d'élément à enlever

Détermine si un tableau contient l'élément
```js
monTableau.includes(10)
```
Retrouver un élément
```js
monTableau.find(element => element === "Bob")
```
> Ici je récupère l'élément **Bob** de mon tableau (et **undefined** s'il n'exite pas)

Retrouver l'index d'un élément
```js
monTableau.findIndex(element => element === "Bob")
```
> Renvoi **-1** s'il n'existe pas

Filter un tableau
```js
monTableau.filter(element => element !== "Bob")
```
> Ici je ne garde que les éléments qui ne sont pas Bob

Transformer un tableau
```js
// Déclaration d'un tableau d'objet
const monTableau = [{
    nom: "Bob",
    age: "18"
}, {
    nom: "Lea",
    age: "19"
}]
monTableau.map(element => element.nom)
```
> Mon tableau est maintenant `['Bob', 'Lea']`

#### Naviguer dans un tableau

Avec un **for** simple
```js
const tab = [1, 2, 4, 5];
for(let i = 0; i < tab.length; i++) {
    console.log(tab[i]);
}
```
Avec un for en iteration
```js
for (let item of tab) {
    console.log(item);
}
```

Avec la fonction définie pour les tableaux (conseillé)
```js
tab.forEach((item) => {
    console.log(item);
})
 ```


 ## Fonctions

 ``` js
 
 // Avec le mot clé function
 function addition(nombre1, nombre2) {
     return nombre1 + nombre2
 }
 addition(2, 4);

// Comme une variable
 const addition = (nombre1, nombre2) => {
     return nombre1 + nombre2
 }
 addition(2, 4);

// Peut directement retourner le résultat
// Les parenthèses ne sont pas nécessaires s'il n'y a qu'un paramètre
 const addition = (nombre1, nombre2) => nombre1 + nombre2
 addition(2, 4);

 // Une fonction peut être anonyme
 function fonctionAppel(maFonction) {
     maFonction()
 }
 fonctionAppel(() => {
     // Je fais quelque chose
 })
 ```

## Classe

Définition d'une classe
```js
class MaClasse {
    constructor(obj) {
        this.obj = obj
    }

    getObject() {
        return this.obj
    }
}

const monObjet = new MaClasse({nom: 'Bob'})
```

Une classe peut hériter d'une autre classe
```js
class MonEnfant extends MaClasse {
    constructor(obj, obj2) {
        super(obj)
        this.obj2 = obj2
    }

    methodeEnfant() {
        // Fait quelque chose
    }
}
```

## Avancé

```js
const monObjet = {nom: 'Bob', age: 18}
const {nom, age} = monObjet

const monTableau = ['Bob', 'Lea']
const [bob, lea] = monTableau
```

### Spread operator
Sur les fonctions
```js
function log(...elements) {
    // elements est un tableau
}
log('oui', 'non', 'un texte')

function maFonction(arg1, arg2, arg3){}
const mesArguments = [1, 2, 3] 
maFonction(...mesArgument)
// Correspond à maFonction(mesArguments[0], mesArguments[1], mesArguments[2])
// Soit maFonction(1, 2, 3)
```
Sur les tableaux
```js
const tableau1 = [1, 2, 3]
const tableau2 = [4, 5, 6]
const tableau3 = [...tableau1,  ...tableau2, 7, 8, 9]
// tableau3 est [1, 2, 3, 4, 5, 6, 7, 8, 9]
```
Sur les objets
```js
const bob = {
    nom: 'Bob',
    age: 18
}
const gout = {
    aimeLesFrites: true
}
const bobComplet = {...bob, ...gout}
```


## Equal

Penser à `===`

```js
1 === 1 // true
1 == 1 // true
1 == '1' // true
1 === '1'// false
```

[exemples](https://developer.mozilla.org/fr/docs/Web/JavaScript/Les_diff%C3%A9rents_tests_d_%C3%A9galit%C3%A9)

