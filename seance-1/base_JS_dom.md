# Interaction avec le DOM

## Récupération d'un élément du DOM

Par id
```js
document.getElementById('monId')
```
Par classe
```js
document.getElementsByClassName('ma-classe')
```
> Il retourne une liste d'élément

Selecteur custom
```js
// Récupère le premier élément
document.querySelector('#monId.ma-classe')

// Récupère tous les éléments
document.querySelectorAll('#monId.ma-classe')
```

## Ajout dans le DOM
Créer un noeud 
```js
const node = document.createElement("p");
```
Créer un noeud texte
```js
const textNode = document.createTextNode("Mon texte");
```

Ajout un noeud à un autre
```js
node.appendChild(textNode);
```
Enlever un noeud
```js
node.removeChild(textNode)
```

## Element d'un noeud
Accéder aux noeuds alentours
```js
// Dernier noeud
node.lastChild

// Premier noeud
node.firstChild

// Parent du noeud
node.parentNode

// Enfants du noeud
node.children
```


Accéder / Modifier les éléments du noeud
```js
// Id du noeud
node.id
// Liste de classes (string)
node.className 
// Liste des classes (liste de string)
node.classList
// Accéder au style  du noeud
node.style
// Change la couleur de fond du noeud en bleu
node.style.backgroundColor = 'blue'

// Contenu du noeud
node.innerText
node.innerHTML
```

## Événement

Directement en html
```html
<div>
    <button onclick="handleClick">Click here</button>
</div>
<script>
    function handleClick(event) {
        
    }
</script>
```

Associer à un noeud
```js
const node = document.getElementById('monId')
node.onClick = (event) => {

}
```
Avec un eventListener
```js
const node = document.getElementById('monId')
node.addEventListener("click", event => {}
```

> `event` permet d'avoir des informations sur l'événement
```js
// Noeud ayant déclenché l'action
event.target

// Type de l'événement
event.type

// Empêche l'action par défaut (par exemple un submit sur un form)
event.preventDefault()

// Empêche la propagation
event.stopPropagation()
```

Type d'evenement:
- click  : clique sur l'éléménent
- submit : envoi d'un formulaire
- change : chnagement de l'élément (utile pour récupérer un `input`)
- focus  : l'élément prend le focus
- blur   : l'élément perd le focus

