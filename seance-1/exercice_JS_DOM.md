# Exercices Séance 2 sur le DOM

## Todo
Le but de l'exercice va être de créer une page permettant de gérer les tâches

![TODO](todo.gif)

1. Ajouter dans une fichier HTML un `input`, un `button` et une `div` qui contiendra toutes les tâches
1. Lors du clique du bouton, afficher dans la console le contenu de l'`input`
1. Ajouter une tâche lors du clique du bouton
1. Supprimer la tâche en cliquant dessus
1. Maintenant, afficher devant chaque tâche 'Check'
1. Cliquer sur ce texte finit la tâche, changer alors le style de la tâche en la passant en gris et en la barrant.
1. Lorsqu'une tâche est finie, le texte à gauche est maintenant 'Uncheck'
1. Cliquer sur une tâche finie la remet en état non finie
1. Passer en formulaire afin de pouvoir ajouter une tâche en appuyant sur entrée
1. Remplacer le check/uncheck par une image (vous pouver utiliser [cette icône](https://material.io/resources/icons/?search=check&icon=done&style=baseline))
1. Changer la couleur de l'image lorsque la tâche est finie
1. Ajouter un texte permettant d'avoir le nombre de tâche
1. Ajouter un texte permettant d'avoir des infos sur les tâches
    1. Le nombre de tâche non finie
    1. Le pourcentage de tâche finie

![TODO improved](todo_advanced.gif)