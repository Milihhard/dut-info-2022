import axios from 'axios'

const apiUrl = 'http://localhost:3001';

export const getTasks = async () => {
    const res = await axios.get(`${apiUrl}/tasks`);
    return res.data
}

export const addTask = async (name) => {
    const res = await axios.post(`${apiUrl}/tasks`, {
        name
    })
    return res.data
}

export const updateTaskState = async (task) =>
    await axios.put(`${apiUrl}/tasks/${task.id}`, {
        state: !task.state
    });

export const deleteTask = async (task) =>
    await axios.delete(`${apiUrl}/tasks/${task.id}`);