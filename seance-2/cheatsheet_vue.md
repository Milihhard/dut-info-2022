# Vue

## Créer un projet vue
```sh
# Création du projet Vue 'project-name'
npm create vite
```
![configuration Vite](./vite.png)

## Créer un composant
### Création du composant (script)
```html
<script>
export default {
  // Nom du composant
  name: 'nom',
  // Composants utilisés
  components: {
    Component1,
    Component2,
  },
  // Données du composant
  data: () => ({ info: '', tab: [], buttonType: 'submit' }),
  // Données calculées
  computed: {
    calcul() {
      return 0
    },
    // Méthodes du composant
    methods: {
      maFonction() {
        // Do something
        // On accède aux éléments du composant avec this
        this.info = 'info'
        // Envoie un  événement 'output' au parent avec comme argument 10
        this.$emit('output', 10)
      }
    }
  }
}
</script>
```
### Création du composant (HTML)
```html
<template>
  <!-- /!\ Un seul enfant au template -->
  <div class="component">
    <h1>Mon composant</h1>

    <!-- Correspond à info dans les données du composant -->
    <p>Mon info: {{info}}</p>
    <!-- Correspond à calcul dans les données calculées du composant -->
    <!-- v-if pour la condition de rendu -->
    <p v-if="info">Variable calculé: {{calcul}}</p>

    <!-- : Correspond à une variable passée à l'enfant -->
    <!-- @ Correspond à un événement (ici un clique) -->
    <button :type="buttonType" @click="maFonction">Appuyer sur le bouton</button>
    <!-- v-model permet de lier un model avec un input -->
    <input v-model="info" placeholder="Information"/>
    <!-- v-for pour rendre plusieurs fois -->
    <div v-for="item in tab">tab</div>
  </div>
</template>
```

### Création du composant (style)
```html
<!-- scoped: le style est définit que pour le composant -->
<style scoped>
.component {
  background-color: gray
}
</style>
```

## Gérer le style par Vue
### A l'aide de classes
```html
<template>
  <div :class="{ 'ma-classe-dynamique': true }">
  </div>
</template>
```
### A l'aide de style
```html
<template>
  <div :style="{ 'background-color': true === true ? 'blue' : 'lime' }">
  </div>
</template>
```